export default function() {
  let width = screen.width;
  if (window.innerWidth > 0) {
    width = window.innerWidth;
  }
  return width < 480;
}
