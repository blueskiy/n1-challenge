'use strict';

import '../css/style.scss';
import isMobile from './utils/is-mobile';

/********** Paste your code here! ************/

$('#menu-hamburguer').click(function() {
  if (!isMobile()) {
    $('#menu-hamburguer--inactive').toggle('fast', function() {});
    return;
  }

  $('#menu-hamburguer--inactive').toggleClass('active');
});

$('.menu-link').click(function() {
  $('#menu-hamburguer--inactive').toggleClass('active');
});

$('.banner-slider').slick({
  dots: false,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear'
});

function getProducts() {
  return $.get('products.json').then(function(data) {
    console.log(data);
    let shelf = data.map(function({ image, name, price }) {
      return `
      <div class="product-layout">
        <img src="${image}" class="product-image" />
        <p>${name}</p>
        <h3>R$ ${price.toFixed(2)}</h3>
        <a href="#modal" rel="modal:open">
          <button class="buy">Comprar</button>
        </a> 
      </div>
      `;
    });
    $('.shop-items').html(shelf.join(''));

    $('.buy').click(function() {
      $('#bag-count').show();
      const prevCount = $('#count').text();
      $('#count').text(Number(prevCount) + 1);
    });
  });
}

$(document).ready(function() {
  console.log('ready!');
  const productPromise = getProducts();
  if (isMobile()) {
    console.log('é mobile maluco');
    productPromise.then(function() {
      $('.shop-items').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
      });
    });
  }
});
